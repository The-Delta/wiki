<?php
/*
 * Dokuwiki's Main Configuration File - Local Settings
 * Auto-generated by config plugin
 * Run for user: lanodan
 * Date: Mon, 25 Aug 2014 23:25:39 +0200
 */

$conf['title'] = 'The-Delta';
$conf['lang'] = 'fr';
$conf['template'] = 'kiss';
$conf['license'] = 'cc-by-sa';
$conf['breadcrumbs'] = 0;
$conf['youarehere'] = 1;
$conf['useacl'] = 1;
$conf['autopasswd'] = 0;
$conf['superuser'] = '@admin,@staff';
$conf['subscribers'] = 1;
$conf['htmlmail'] = 0;
$conf['sitemap'] = 1;
$conf['rss_type'] = 'rss2';
$conf['updatecheck'] = 0;
$conf['useslash'] = 1;
$conf['compress'] = 0;
$conf['gzip_output'] = 1;
$conf['send404'] = 1;
$conf['dnslookups'] = 0;
$conf['plugin']['authldap']['server'] = 'localhost';
$conf['plugin']['gallery']['thumbnail_width'] = 128;
$conf['plugin']['gallery']['thumbnail_height'] = 128;

// end auto-generated content
