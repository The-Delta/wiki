    <footer>
<?php
  // must be run from within DokuWiki
  if (!defined('DOKU_INC')) die();
?>
<?php tpl_license(''); // license text ?>
<?php
  tpl_license('button', true, false, false); // license button, no wrapper
  $target = ($conf['target']['extern']) ? 'target="'.$conf['target']['extern'].'"' : '';
?>
      <a href="http://www.catb.org/hacker-emblem/" title="Hacker Emblem" <?=$target?>><img src="<?=tpl_basedir();?>images/button-glider.png" width="80" height="15" alt="glider"/></a>
      <a href="http://www.php.net" title="Powered by PHP" <?=$target?>><img src="<?=tpl_basedir();?>images/button-php.png" width="80" height="15" alt="PHP Powered"/></a>
      <a href="http://anybrowser.org/campaign/index.html" title="Viewable on Anybrowser" <?=$target?>><img src="<?=tpl_basedir();?>images/button-anybrowser.png" width="80" height="15" alt="AnyBrowser"/></a>
      <a href="http://validator.w3.org/check/referer" title="Valid HTML5" <?=$target?>><img src="<?=tpl_basedir();?>images/button-html5.png" width="80" height="15" alt="W3C HTML"/></a>
      <a href="http://jigsaw.w3.org/css-validator/check/referer?profile=css3" title="Valid CSS" <?=$target?>><img src="<?=tpl_basedir();?>images/button-css.png" width="80" height="15" alt="W3C CSS"/></a>
      <a href="http://dokuwiki.org/" title="Driven by DokuWiki" <?=$target?>><img src="<?=tpl_basedir();?>images/button-dw.png" width="80" height="15" alt="DokuWiki"/></a>
      <p>Serveur hébergé sur un <a href="//twitter.com/derpi_">grille-pain</a>.</p>
    </footer>
