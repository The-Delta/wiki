<?php
/**
 * DokuWiki Image Detail Page
 *
 * @author   Andreas Gohr <andi@splitbrain.org>
 * @author   Anika Henke <anika@selfthinker.org>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

// must be run from within DokuWiki
if (!defined('DOKU_INC')) die();
?><!DOCTYPE html>
<html lang="<?php echo $conf['lang']?>" dir="<?php echo $lang['direction'] ?>">
  <head>
    <meta charset="utf-8" />
    <title>
      <?=hsc(tpl_img_getTag('IPTC.Headline',$IMG))?>
      [<?=strip_tags($conf['title'])?>]
    </title>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?php tpl_metaheaders()?>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <?=tpl_favicon(array('favicon', 'mobile')) ?>
    <?php tpl_includeFile('meta.html') ?>
  </head>
  <body>
    <div id="dokuwiki__site" class="site <?php echo tpl_classes(); ?>">
      <?php include('tpl_header.php') ?>
      <div class="wrapper group" id="dokuwiki__detail">
        <!-- ********** CONTENT ********** -->
        <div id="dokuwiki__content" class="pad group">

          <?php if(!$ERROR): ?>
          <div class="pageId"><span><?php echo hsc(tpl_img_getTag('IPTC.Headline',$IMG)); ?></span></div>
          <?php endif; ?>

          <div class="page group">
            <?php tpl_flush() ?>
            <?php tpl_includeFile('pageheader.html') ?>
            <!-- detail start -->
            <?php
              if($ERROR):
                echo '<h1>'.$ERROR.'</h1>';
              else: ?>
	        <h1><?php echo nl2br(hsc(tpl_img_getTag('simple.title'))); ?></h1>
                <?php tpl_img(900,700); /* parameters: maximum width, maximum height (and more) */ ?>
		<div class="img_detail">
		  <?php tpl_img_meta(); ?>
		</div>
                <?php //Comment in for Debug// dbg(tpl_img_getTag('Simple.Raw'));?>
              <?php endif; ?>
          </div>
          <!-- detail stop -->
          <?php tpl_includeFile('pagefooter.html') ?>
          <?php tpl_flush() ?>
          <?php
            /* doesn't make sense like this; @todo: maybe add tpl_imginfo()?
             * <div class="docInfo"><?php tpl_pageinfo(); ?></div>
             */
          ?>
        </div>
      </div>
      <hr class="a11y" />
      <!-- PAGE ACTIONS -->
      <?php if (!$ERROR): ?>
        <section id="dokuwiki__pagetools">
	  <h3 class="a11y"><?php echo $lang['page_tools']; ?></h3>
	  <ul class="tools">
            <?php
              $data = array(
                'view' => 'detail',
                'items' => array(
                  'mediaManager' => tpl_action('mediaManager', 1, 'li', 1, '<span>', '</span>'),
                  'img_backto' =>   tpl_action('img_backto',   1, 'li', 1, '<span>', '</span>'),
                  )
                );
	      // the page tools can be amended through a custom plugin hook
	      $evt = new Doku_Event('TEMPLATE_PAGETOOLS_DISPLAY', $data);
	      if($evt->advise_before()) {
		foreach($evt->data['items'] as $k => $html) echo $html;
              }
              $evt->advise_after();
              unset($data);
              unset($evt);
            ?>
          </ul>
        </section>
      <?php endif; ?>
    </div>
    <?php include('tpl_footer.php') ?>
  </body>
</html>
