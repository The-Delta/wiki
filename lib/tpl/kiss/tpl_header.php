<?php
// must be run from within DokuWiki
if (!defined('DOKU_INC')) die();
tpl_includeFile('header.html');

// get logo either out of the template images folder or data/media folder
$logoSize = array();
$logo = tpl_getMediaFile(array(':wiki:logo.png', ':logo.png', 'images/logo.png'), false, $logoSize);
 ?>
    <header>
      <div class="headings group">
        <h1 id="title"><?php tpl_link(wl(), '<img src="'.$logo.'" '.$logoSize[3].' alt="'.$conf['title'].'" />'.$conf['title']); ?></h1>
<?php if ($conf['tagline']): ?>
        <p class="claim"><?=$conf['tagline']?></p>
<?php endif ?>
      </div>
      <div class="tools">
<?php if ($conf['useacl']): ?>
        <section id="dokuwiki__usertools">
          <h3><?=$lang['user_tools']?></h3>
<?php
  if (!empty($_SERVER['REMOTE_USER']) ) {
    print '          <div class="userinfo">';
    tpl_userinfo();
    print '</div>'."\n";
  }
  print '          '.tpl_actionlink('admin', '', '', '', 1)."\n";
  print '          '.tpl_actionlink('profile', '', '', '', 1)."\n";
  print '          '.tpl_actionlink('register', '', '', '', 1)."\n";
  print '          '.tpl_actionlink('login', '', '', '', 1)."\n";
?>
        </section>
<?php endif ?>
        <section id="dokuwiki__sitetools">
          <h3><?=$lang['site_tools']?></h3>
          <form action="<?=wl()?>" class="search" id="dw__search" method="get" role="search">
            <input type="hidden" name="do" value="search"/>
            <input type="text" id="qsearch__in" accesskey="f" name="id" class="edit" title="[F]"/>
            <input class="button" title="<?=$lang['btn_search']?>" type="submit"/>
          </form>
<?php
  print '          '.tpl_actionlink('recent', '', '', '', 1)."\n";
  print '          '.tpl_actionlink('media', '', '', '', 1)."\n";
  print '          '.tpl_actionlink('index', '', '', '', 1)."\n";
?>
        </section>
      </div>
<?php if($conf['youarehere']): ?>
      <div class="breadcrumbs youarehere"><?php tpl_youarehere() ?></div>
<?php endif ?>
<?php if($conf['breadcrumbs']): ?>
      <div class="breadcrumbs trace"><?php tpl_breadcrumbs() ?></div>
<?php endif ?>
<?php html_msgarea() ?>
    </header>
