<!DOCTYPE html>
<?php
  /**
   * DokuWiki KISS Template
   *
   * @link     http://dokuwiki.org/template
   * @author   Haelwenn Monnier (lanodan) <lanodan.delta@free.fr>
   * @license  CC-BY-SA 4.0 International [https://creativecommons.org/licenses/by-sa/4.0/] 
   */
  if (!defined('DOKU_INC')) die(); /* must be run from within DokuWiki */
  $hasSidebar = page_findnearest($conf['sidebar']);
  $showSidebar = $hasSidebar && ($ACT=='show');
?>
<html lang="<?=$conf['lang']?>" dir="<?=$lang['direction']?>">
  <head>
    <meta charset="utf-8" />
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?=tpl_favicon(array('favicon')) ?>
    <title><?php tpl_pagetitle() ?> - <?=strip_tags($conf['title']) ?></title>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
<?php tpl_metaheaders() ?>
  </head>
  <body>
<?php include('tpl_header.php') ?>
<?php if($showSidebar): ?>
    <aside>
      <h3><?php echo $lang['sidebar'] ?></h3>
<?php 
  tpl_flush();
  tpl_includeFile('sidebarheader.html');
  tpl_include_page($conf['sidebar'], 1, 1);
  tpl_includeFile('sidebarfooter.html');
 ?>
    </aside>
<?php endif; ?>
<?php tpl_flush() ?>
      <nav class="pagetools">
<?php
  print '        '.tpl_actionlink('edit', '', '', '', 1)."\n";
  print '        '.tpl_actionlink('revert', '', '', '', 1)."\n";
  print '        '.tpl_actionlink('revisions', '', '', '', 1)."\n";
  print '        '.tpl_actionlink('backlink', '', '', '', 1)."\n";
  print '        '.tpl_actionlink('subscribe', '', '', '', 1)."\n";
?>
      </nav>
    <div id="content">
      <?php tpl_content() ?>
      <div class="docInfo"><?php tpl_pageinfo() ?></div>
      <?php tpl_flush() ?>
    </div>
<?php include('tpl_footer.php') ?>
  </body>
</html>
